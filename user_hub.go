package main

import (
	"crypto/sha1"
	"fmt"
	"log"
	"sync"
)

type User struct {
	Id       int64 `gorethink:"id,omitempty"`
	Boost    float64
	Balance  float64
	IsActive bool
	Token    string
}

type UserChangeSet struct {
	NewVal User `gorethink:"new_val,omitempty"`
	OldVal User `gorethink:"old_val,omitempty"`
}

type UserHub struct {
	sync.RWMutex

	usersByConnection map[*connection]bool
}

var globalUserHub = &UserHub{
	usersByConnection: make(map[*connection]bool),
}

func (h *UserHub) AddByConnectionAsync(c *connection) {
	go func() {
		h.AddByConnection(c)
	}()
}

func (h *UserHub) AddByConnection(c *connection) {
	t := convertConnectionToToken(c)
	log.Println("token =", t)
	h.Lock()
	defer h.Unlock()
}

const shaPrefix = 2

func convertConnectionToToken(c *connection) string {
	s := fmt.Sprintf("%v", c)
	sum := sha1.Sum([]byte(s))
	return fmt.Sprintf("%x", sum[:shaPrefix])
}
