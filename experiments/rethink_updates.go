package main

import (
	"flag"
	"log"
	"math/rand"
	"time"

	"github.com/dancannon/gorethink"
)

var rowsCnt = flag.Int("rows", 5000, "insert rows count")

type User struct {
	Id       int64 `gorethink:"id,omitempty"`
	Boost    float64
	Balance  float64
	Assets   []Asset
	IsActive bool
}

type Asset struct {
	Id    int64
	Count int64
	Boost float64
}

const table = "users0"

func main() {
	flag.Parse()

	rDB, err := gorethink.Connect(gorethink.ConnectOpts{Address: "localhost:28015"})
	if err != nil {
		log.Fatal("Database connection failed:", err)
	}
	defer rDB.Close()
	insertRows(rDB)

	tick := time.NewTicker(time.Second)

	updateFilter := map[string]interface{}{
		"IsActive": true,
	}
	updateQuery := map[string]interface{}{
		//"status": "published",
		"Balance": gorethink.Row.Field("Balance").Add(gorethink.Row.Field("Boost")),
		//r.row("Balance").add(r.row('Boost')
	}
	for {
		select {
		case <-tick.C:
			log.Println("tick start")
			//rDB.Table(table)
			res, updateErr := gorethink.Table(table).Filter(updateFilter).Update(updateQuery).RunWrite(rDB)
			if updateErr != nil {
				panic(updateErr)
			}
			log.Printf("updated %v\n", res)
			//Update(user, upsertOn).RunWrite(db)
			log.Println("update end")
		}
	}
	// update
}

func insertRows(db *gorethink.Session) {
	for i := 0; i < *rowsCnt; i++ {
		aCount := rand.Int31n(3)
		assets := make([]Asset, aCount)
		for j := 0; j < int(aCount); j++ {
			assets[j].Count = int64(rand.Int31n(3))
			assets[j].Boost = float64(aCount % 2 * aCount)
		}
		user := User{
			Id:       int64(i),
			Boost:    100,
			Balance:  0,
			Assets:   assets,
			IsActive: rand.Int31n(3)%2 == 0,
		}

		upsertOn := gorethink.InsertOpts{Conflict: "replace"}
		_, err := gorethink.Table(table).Insert(user, upsertOn).RunWrite(db)
		if err != nil {
			panic(err)
		}
	}
}
