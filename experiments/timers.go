package main

import (
	"flag"
	"log"
	"math/rand"
	"sync"
	"time"
)

// import "time"

var timersCnt = flag.Int("timers", 5000, "create timers")

type User struct {
	Id      int64
	Boost   int64
	Balance int64
}

type UserNode struct {
	ticker *time.Ticker
	user   User
}

func main() {
	flag.Parse()
	log.Printf("Create %v timers", *timersCnt)

	nodes := []UserNode{}
	var wg sync.WaitGroup
	wg.Add(1)
	for i := 0; i < *timersCnt; i++ {
		node := UserNode{
			ticker: time.NewTicker(time.Second),
			user: User{
				Id:      int64(i),
				Boost:   100,
				Balance: 0,
			},
		}
		nodes = append(nodes, node)
		// for timers distribution
		time.Sleep(time.Duration(rand.Int31n(100)) * time.Microsecond)
		// if i % 100 == 0 {
		// time.Sleep(100 * time.Millisecond)
		// }
		go func(n UserNode) {
			wg.Wait()
			for {
				select {
				case <-node.ticker.C:
					node.user.Balance += node.user.Boost
					if node.user.Balance%1000 == 0 {
						time.Sleep(time.Duration(rand.Int31n(10000)) * time.Microsecond)
						// log.Printf("%v balance %v", node.user.Id, node.user.Balance)
					}
				}
			}
		}(node)
	}
	log.Println("all timers created")
	time.Sleep(time.Second * 1)
	log.Println("ready? steady?! go!")
	wg.Done()
	time.Sleep(time.Second * 10000)
}
