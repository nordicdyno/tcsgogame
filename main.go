package main

import (
	"flag"
	"log"
	"net/http"
	"time"

	"github.com/dancannon/gorethink"
	"github.com/labstack/echo"

	"github.com/gorilla/websocket"
	"github.com/labstack/echo/engine/standard"
	"github.com/labstack/echo/middleware"
)

var (
	addr = flag.String("addr", ":1323", "http service address")
	// serve = flag.String("serve", "", "serve dir with templates")
	Debug = flag.Bool("debug", false, "print a lot!")
)

const table = "users0"

func main() {
	flag.Parse()

	var dbErr error
	rDB, dbErr = gorethink.Connect(gorethink.ConnectOpts{Address: "localhost:28015"})
	if dbErr != nil {
		log.Fatal("Database connection failed:", dbErr)
	}
	// defer rDB.Close()

	e := echo.New()
	// e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	// e.Use(middleware.Static("../public"))
	e.GET("/ws", standard.WrapHandler(http.HandlerFunc(wsHandlerFunc())))

	go messageHub.run()
	log.Println("start server on", *addr)
	e.Run(standard.New(*addr))
}

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	// HandshakeTimeout specifies the duration for the handshake to complete.
	HandshakeTimeout: time.Second * 3,
	CheckOrigin:      func(r *http.Request) bool { return true },
}

func wsHandlerFunc() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		log.Println("wsHandler fired")
		// hdr["Access-Control-Allow-Origin"] = []string{"*"}

		ws, err := upgrader.Upgrade(w, r, nil)
		if err != nil {
			log.Print("upgrade failed:", err)
			return
		}
		// defer ws.Close()

		c := &connection{
			send: make(chan []byte, 10), // limit queue to 10 messages
			ws:   ws,
			// nc:   natsConnect(),
		}
		messageHub.register <- c
		defer func() {
			messageHub.unregister <- c
			ws.Close()
			log.Println("close connection")
		}()

		log.Println("connection started")
		go c.writer()
		c.reader()
	}
}
